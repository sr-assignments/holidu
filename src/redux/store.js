import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

import search from "./search/reducer";

function store(initialState = {}) {
  const store = createStore(
    combineReducers({ search }),
    initialState,
    compose(applyMiddleware(thunk))
  );

  return store;
}

export default store;
