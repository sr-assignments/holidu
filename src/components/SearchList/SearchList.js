import "./SearchList.css";

import React from "react";
import { Layout, Typography } from "antd";

import Offer from "./Offer";

const { Header, Content } = Layout;
const { Title } = Typography;

function SearchList(props) {
  const { getSearchResults, offers, isLoading } = props;

  React.useEffect(() => {
    getSearchResults();
    // empty deps acts like componentWillMount
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Layout>
        <Header className="Header">
          <Title level={2} style={{ color: "white", marginTop: 10 }}>
            Hotels List
          </Title>
        </Header>
        {isLoading ? (
          <p>Loading...</p>
        ) : (
          <Content className="Content">
            {offers.map((offer, index) => (
              <Offer key={index} {...offer} />
            ))}
          </Content>
        )}
      </Layout>
    </>
  );
}

export default SearchList;
