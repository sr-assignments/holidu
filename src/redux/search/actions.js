import { createAction } from "redux-actions";

export const getSearchResultsRequest = createAction(
  "GET_SEARCH_RESULTS_REQUEST"
);
export const getSearchResultsFailure = createAction(
  "GET_SEARCH_RESULTS_FAILURE",
  (error) => ({ error })
);
export const getSearchResultsSuccess = createAction(
  "GET_SEARCH_RESULTS_SUCCESS",
  ({ metaData, offers }) => ({ metaData, offers })
);

export const getSearchResults = () => (dispatch) => {
  dispatch(getSearchResultsRequest());
  return fetch(
    "https://api.holidu.com/old/rest/v6/search/offers?searchId=46fdd790-e0a7-4b96-98b5-6be594a8e8b1&searchTerm=Mallorca%2C+Spanien&adults=2&domainId=2&locale=de-DE&currency=EUR"
  )
    .then((res) => res.json())
    .then(
      (data) => dispatch(getSearchResultsSuccess(data)),
      (e) => dispatch(getSearchResultsFailure(e))
    );
};
