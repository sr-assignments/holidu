import { handleActions } from "redux-actions";
import * as actions from "./actions";

const initialState = {
  idLoading: true,
  offers: [],
  metaData: {},
  error: {},
};

const reducer = handleActions(
  {
    [actions.getSearchResultsRequest]: (state) => ({
      ...state,
      isLoading: true,
    }),
    [actions.getSearchResultsSuccess]: (state, action) => ({
      ...state,
      isLoading: false,
      metaData: action.payload.metaData,
      offers: action.payload.offers,
    }),
    [actions.getSearchResultsFailure]: (state, action) => ({
      ...state,
      isLoading: false,
      error: action.payload.error,
    }),
  },
  initialState
);

export default reducer;
