import "./App.css";

import React, { Component } from "react";
import { Provider } from "react-redux";

import store from "./redux/store";
import SearchList from "./components/SearchList";
export default class App extends Component {
  constructor(props) {
    super(props);
    this._store = store();
  }

  render() {
    return (
      <Provider store={this._store}>
        <SearchList />
      </Provider>
    );
  }
}
