import "./Offer.css";

import React from "react";
import { Card, Image, Typography, Button } from "antd";
import {
  FaMapMarkerAlt,
  FaStar,
  FaStarHalfAlt,
  FaRegStar,
  FaChevronRight,
} from "react-icons/fa";
import { BiEuro } from "react-icons/bi";
import LazyLoad from "react-lazyload";

const { Text, Title, Paragraph } = Typography;

const uspUnitMapping = {
  USP_PEOPLE: "pers.,",
  USP_BED_ROOM: "bedroom,",
  USP_AREA: "ft",
};

function Offer(props) {
  const {
    price,
    location,
    photos,
    provider,
    usps,
    cancellationPolicy,
    rating,
  } = props;

  const getRatingValue = () => {
    const ratingValue = rating.value / (2 * 10);
    return ratingValue.toFixed(1);
  };

  return (
    <LazyLoad offset={300}>
      <Card className="OfferCard">
        <div className="CardWrap">
          <div className="offer-left-section">
            <div className="Offer">
              <Image src={photos[0] && photos[0].t} />
              <div className="MainWrapper">
                <div className="details-top">
                  <Title level={2}>{provider && provider.legalName}</Title>
                  <div className="LocationSection">
                    <FaMapMarkerAlt />
                    <Text>{location.name}</Text>
                  </div>
                  <div className="HotelDetailSection">
                    {usps &&
                      usps.map((uspData, uspIndex) => {
                        return (
                          <Text>
                            {uspData.value} {uspUnitMapping[uspData.id]}
                            {uspData.id === "USP_AREA" && <sup>2</sup>}
                          </Text>
                        );
                      })}
                  </div>
                  <div className="CancelSection">
                    <Text>{`${
                      cancellationPolicy ? "FREE" : "NO"
                    } cancellation`}</Text>
                  </div>
                </div>
                <div className="details-bottom">
                  <div className="RatingSection">
                    <div className="RatingLeft">
                      <Paragraph className="RatingText">
                        {getRatingValue()}
                      </Paragraph>
                      <div className="RatingStars">
                        {Array.apply(null, { length: 5 }).map(
                          (rating, ratingIndex) => {
                            const ratingValue = getRatingValue();
                            if (ratingIndex <= Math.floor(ratingValue - 1))
                              return <FaStar />;
                            else if (
                              ratingIndex > Math.floor(ratingValue - 1) &&
                              ratingIndex <= Math.ceil(ratingValue - 1)
                            ) {
                              return <FaStarHalfAlt />;
                            }
                            return <FaRegStar />;
                          }
                        )}
                      </div>
                    </div>
                    <Text>({rating.count} Reviews)</Text>
                  </div>
                  <Button type="text" className="MoreBtn">
                    <Paragraph>More Details</Paragraph>
                    <FaChevronRight />
                  </Button>
                </div>
              </div>
            </div>
          </div>
          <div className="PriceWrap">
            <div className="PriceText">
              <Paragraph className="PriceText">from</Paragraph>
              <div className="PriceValue">
                {price.currency && <BiEuro />}
                <span>{price.daily}</span>
              </div>
              <Paragraph>per night</Paragraph>
            </div>
            <div className="OfferSection">
              <Paragraph>
                <b>Best Price:</b> vrbo
              </Paragraph>
              <Button type="primary" danger className="OfferBtn">
                VIEW OFFER
              </Button>
            </div>
          </div>
        </div>
      </Card>
    </LazyLoad>
  );
}

export default Offer;
