import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import SearchList from "./SearchList";
import { getSearchResults } from "../../redux/search/actions";

const mapStateToProps = (state) => state.search;
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ getSearchResults }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SearchList);
